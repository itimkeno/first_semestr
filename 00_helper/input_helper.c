#include "input_helper.h"

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <string.h>

void flush_stdin(void)
{
    int trash_char;
    while ((trash_char = getchar()) != '\n' && trash_char != EOF)
    {
    }
    return;
}

// return success as boolean (0, 1), on success write result through *number:
int safe_input_integer(int* number)
{
    long integer_number;
    char buf[32]; // allocate buffer

    if (!fgets(buf, 32, stdin))
    {
        // reading input failed:
        return FALSE;
    }

    // have some input, convert it to integer:
    char* endptr;
    // reset error number
    errno = 0;

    integer_number = strtol(buf, &endptr, 10);
    if (errno == ERANGE)
    {
        // out of range for a long
        return FALSE;
    }
    if (endptr == buf)
    {
        // no character was read
        return FALSE;
    }
    if (*endptr && *endptr != '\n')
    {
        // *endptr is neither end of string nor newline,
        // so we didn't convert the *whole* input
        return FALSE;
    }
    if (integer_number > INT_MAX || integer_number < INT_MIN)
    {
        // result will not fit in an int
        return FALSE;
    }

    // write result through the pointer passed
    *number = (int)integer_number;
    return TRUE;
}

// return success as boolean (0, 1), on success write result through *number:
int safe_input_unsigned_integer(size_t* number)
{
    unsigned long unsigned_number;
    char          buf[32];

    if (!fgets(buf, 32, stdin))
        return FALSE;

    char* endptr;
    errno = 0;
    // unsigned can't contain minus sumbol
    if (strspn(buf, "-"))
        return FALSE;

    unsigned_number = strtoul(buf, &endptr, 10);
    if (errno == ERANGE)
        return FALSE;

    if (endptr == buf)
        return FALSE;

    if (*endptr && *endptr != '\n')
        return FALSE;

    if (unsigned_number > ULONG_MAX)
        return FALSE;

    *number = (size_t)unsigned_number;
    return TRUE;
}

// return success as boolean (0, 1), on success write result through *number:
int safe_input_real(double* number)
{
    double floating_number;
    char   buf[64];

    if (!fgets(buf, 64, stdin))
        return FALSE;

    char* endptr;
    errno = 0;

    floating_number = strtod(buf, &endptr);
    if (errno == ERANGE)
        return FALSE;

    if (endptr == buf)
        return FALSE;

    if (*endptr && *endptr != '\n')
        return FALSE;

    *number = floating_number;
    return TRUE;
}

double get_real_number_from_stdin(void)
{
    double real_value = 0.0;
    if (!safe_input_real(&real_value))
    {
        fprintf(stderr,
                "Wrong input.\nInput must contain only real numbers.\n");
        fflush(stderr);
        exit(EXIT_FAILURE);
    }
    return real_value;
}

int get_integer_number_from_stdin(void)
{
    int integer_value = 0;
    if (!safe_input_integer(&integer_value))
    {
        fprintf(stderr,
                "Wrong input.\nInput must contain only integer numbers.\n");
        fflush(stderr);
        exit(EXIT_FAILURE);
    }
    return integer_value;
}

size_t get_unsigned_integer_number_from_stdin(void)
{
    size_t unsigned_value = 0;
    if (!safe_input_unsigned_integer(&unsigned_value))
    {
        fprintf(stderr, "Wrong input.\n"
                        "Input must contain only unsigned integer numbers.\n");
        fflush(stderr);
        exit(EXIT_FAILURE);
    }
    return unsigned_value;
}

void clear_screen(void)
{
#ifdef _WIN32
    system("cls");
#elif (__linux__)
    system("clear");
#endif
}

#ifdef _WIN32

/* The original code is public domain -- Will Hartung 4/9/09 */
/* Modifications, public domain as well, by Antti Haapala, 11/10/17
   - Switched to getc on 5/23/19 */

ssize_t getline(char** lineptr, size_t* n, FILE* stream)
{
    size_t pos;
    int    c;

    if (lineptr == NULL || stream == NULL || n == NULL)
    {
        errno = EINVAL;
        return -1;
    }

    c = getc(stream);
    if (c == EOF)
    {
        return -1;
    }

    if (*lineptr == NULL)
    {
        *lineptr = malloc(128);
        if (*lineptr == NULL)
        {
            return -1;
        }
        *n = 128;
    }

    pos = 0;
    while (c != EOF)
    {
        if (pos + 1 >= *n)
        {
            size_t new_size = *n + (*n >> 2);
            if (new_size < 128)
            {
                new_size = 128;
            }
            char* new_ptr = realloc(*lineptr, new_size);
            if (new_ptr == NULL)
            {
                return -1;
            }
            *n       = new_size;
            *lineptr = new_ptr;
        }

        ((unsigned char*)(*lineptr))[pos++] = c;
        if (c == '\n')
        {
            break;
        }
        c = getc(stream);
    }

    (*lineptr)[pos] = '\0';
    return pos;
}
#endif
