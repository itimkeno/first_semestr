#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

#define TRUE 1
#define FALSE 0

double get_real_number_from_stdin(void);
int    get_integer_number_from_stdin(void);
size_t get_unsigned_integer_number_from_stdin(void);
void   clear_screen(void);

#ifdef _WIN32
// if typedef doesn't exist (msvc, blah)
typedef intptr_t ssize_t;
// self implementation of POSIX function
ssize_t getline(char** lineptr, size_t* n, FILE* stream);
#endif
