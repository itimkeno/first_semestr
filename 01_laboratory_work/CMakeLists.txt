cmake_minimum_required(VERSION 3.5)

project(01_laboratory_work LANGUAGES C)

include_directories(${INPUT_HELPER_SOURCE_PATH})

add_executable(01_unit main.c)

if(MSVC)
  target_link_libraries(01_unit PRIVATE input_helper)
elseif(UNIX)
  target_link_libraries(01_unit PRIVATE m
                                        input_helper
                                        )
endif()