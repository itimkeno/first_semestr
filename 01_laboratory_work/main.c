#include <math.h>
#include <stdio.h>

#include "input_helper.h"

double calculate_equation(const double x, const double y, const double z)
{
    double first_argument  = 0.0;
    double second_argument = 0.0;
    double third_argument  = 0.0;
    double fourth_argument = 0.0;
    double check_x_plus_y  = x + y;

    first_argument  = pow(y, pow(fabs(x), 1.0 / 3.0));
    second_argument = cos(y) * cos(y) * cos(y);

    if (check_x_plus_y < 0.0)
    {
        fprintf(stderr,
                "Square root of \"x\" is undefined when \"x\" less then "
                "zero.\n");
        exit(EXIT_FAILURE);
    }
    if (check_x_plus_y == 0.0)
    {
        fprintf(stderr, "Division by zero is undefined.\n"
                        "Error in \"sqrt(x + y)\"\n");
        exit(EXIT_FAILURE);
    }

    third_argument  = fabs(x - y) * (1.0 + ((sin(z) * sin(z)) / sqrt(x + y)));
    fourth_argument = exp(fabs(x - y)) + (x / 2);

    if (fourth_argument == 0.0)
    {
        fprintf(stderr, "Division by zero is undefined.\n"
                        "Error in \"exp(fabs(x - y)) + (x / 2) \"\n");
        exit(EXIT_FAILURE);
    }

    return first_argument + second_argument * third_argument / fourth_argument;
}

int main(void)
{
    printf("Laboratory work 1\nVariant 11\n");

    printf("Please enter x = ");
    double x = get_real_number_from_stdin();
    printf("Please enter y = ");
    double y = get_real_number_from_stdin();
    printf("Please enter z = ");
    double z = get_real_number_from_stdin();

    double result = calculate_equation(x, y, z);

    printf("Answer: b = %.4lf\n", result);

    return EXIT_SUCCESS;
}
