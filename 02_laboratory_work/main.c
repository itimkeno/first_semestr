#include "input_helper.h"

#include <math.h>

int function_choiser(void)
{
    int function_id = get_integer_number_from_stdin();
    if (function_id < 1 || function_id > 3)
    {
        fprintf(stderr, "Function was chosen incorrectly.\n");
        exit(EXIT_FAILURE);
    }
    return function_id;
}

double calculate_function(const int function_id, const double x)
{
    double function_value = 0.0;
    switch (function_id)
    {
        case 1:
        {
            function_value = 2 * x;
            printf("Your choise is: F(x) = 2*x = %lf\n", function_value);
            break;
        }
        case 2:
        {
            function_value = x * x;
            printf("Your choise is: F(x) = x*x = %lf\n", function_value);
            break;
        }
        case 3:
        {
            function_value = x / 3;
            printf("Your choise is: F(x) = x/3 = %lf\n", function_value);
            break;
        }
        default:
        {
            printf("Wrong \"function_id\"");
            exit(EXIT_FAILURE);
        }
    }
    return function_value;
}

int main()
{
    printf("Laboratory work 2\nVariant 11\n");

    printf("Please enter a = ");
    double a = get_real_number_from_stdin();
    printf("Please enter b = ");
    double b = get_real_number_from_stdin();
    printf("Please enter z = ");
    double z = get_real_number_from_stdin();

    double x = 0.0;
    if (z > 0.0)
    {
        x = 1 / (z * z + 2 * z);
        printf("\nWhen \"z\" greater than zero, "
               "x = 1 / (z*z + 2*z) == %lf\n",
               x);
    }
    if (z <= 0.0)
    {
        x = 1 - z * z * z;
        printf("\nWhen \"z\" less than or equal to zero, "
               "x = 1 - z*z*z == %lf\n",
               x);
    }

    printf("\nPlease choise the function:\n"
           "F(x) = 2*x    press 1\n"
           "F(x) = x*x    press 2\n"
           "F(x) = x/3    press 3\n");
    const int function_id = function_choiser();

    double function_value = calculate_function(function_id, x);

    double first_argument  = 2.5 * a * exp(-3.0 * x);
    double second_argument = 4 * b * x * x;
    double third_argument  = log(fabs(x)) + function_value;

    if (third_argument == 0.0)
    {
        fprintf(stderr, "Division by zero is undefined\n");
        exit(EXIT_FAILURE);
    }

    double result = (first_argument - second_argument) / third_argument;
    printf("\nAnswer: y = %.4lf\n", result);

    return EXIT_SUCCESS;
}
