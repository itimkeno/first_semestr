#include <math.h>
#include <stdio.h>

#include "input_helper.h"

// for good answer "n" should be in range [5 : 10]

int main()
{
    double bottom, top, step;
    size_t n;
    printf("Laboratory work 3\nVariant 11\n");
    printf("low limit \"a\" : ");
    bottom = get_real_number_from_stdin();
    printf("hight limit \"b\" : ");
    top = get_real_number_from_stdin();
    printf("step enter \"dh\" : ");
    step = get_real_number_from_stdin();
    printf("iteration for S(x) \"n\" : ");
    n = get_unsigned_integer_number_from_stdin();

    if (bottom > top)
    {
        printf("wrong interval borders\n");
        return EXIT_FAILURE;
    }

    double x = bottom;

    for (double counter = bottom; counter <= top; counter += step)
    {
        double recurrence_relation = 1.0; //(x + x) / 2.0;
        double sum_of_x            = 1.0; // recurrence_relation;
        double y_of_x              = 0.0;
        for (size_t k = 0; k < n; ++k)
        {
            // clang-format off
            recurrence_relation *= ((x * (double)(k + 1) * (double)(k + 1) + x) /
                                   ((double)(k + 1) * (double)(k * k + 1) * 2.0));
            sum_of_x += recurrence_relation;
            // clang-format on
        }

        // TODO :
        // change output to table view

        printf("S(x) = %lf\n", sum_of_x);

        y_of_x = (((x * x) / 4) + ((x / 2) + 1)) * exp(x / 2);
        printf("Y(x) = %lf\n", y_of_x);

        printf("|Y(x) - S(x)| = %lf\n", fabs(y_of_x - sum_of_x));

        x += step;
        printf("\n");
    }
    printf("The end!\n");

    return 0;
}
