cmake_minimum_required(VERSION 3.5)

project(04_laboratory_work LANGUAGES C)

include_directories(${INPUT_HELPER_SOURCE_PATH})

add_executable(04_unit main.c)
target_link_libraries(04_unit PRIVATE input_helper)
