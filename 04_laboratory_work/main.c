#include <stdio.h>

#include "input_helper.h"

size_t find_idx_min_element(const int* array, const size_t size)
{
    int    min   = array[0];
    size_t index = 0;
    for (size_t i = 0; i < size; ++i)
    {
        // if we find two or more equals minimum elements,
        // the last one will be selected
        if (min >= array[i])
        {
            min   = array[i];
            index = i;
        }
    }

    return index;
}

int main(void)
{
    printf("Variant 11\nDificulty 1\n");

    size_t size = 0;
    printf("Enter size of array and fill elemets: ");
    size = get_unsigned_integer_number_from_stdin();

    if (size < 1 || size > 20)
    {
        printf("Array size must be less than 20 and contain at least one "
               "element.\n");
        return EXIT_FAILURE;
    }

    int* array = malloc(size * sizeof(*array));

    for (size_t i = 0; i < size; ++i)
    {
        printf("array[%zu] = ", i);
        array[i] = get_integer_number_from_stdin();
    }

    int    sum_elements_after_min = 0;
    size_t index_of_min_element   = find_idx_min_element(array, size);

    if (index_of_min_element == size - 1)
    {
        printf("Mimimum elements is the last in array\n");
        free(array);
        return EXIT_SUCCESS;
    }

    for (size_t i = index_of_min_element + 1; i < size; ++i)
    {
        sum_elements_after_min += array[i];
    }

    printf("Sum all elements after minimal element equal %d\n",
           sum_elements_after_min);

    free(array);
    return EXIT_SUCCESS;
}
