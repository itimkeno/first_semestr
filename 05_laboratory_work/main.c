#include <stdio.h>

#include "input_helper.h"

int main(void)
{
    printf("Variant 11\nDificulty 1\n");

    size_t input_column = 0;
    size_t input_row    = 0;
    printf("Enter size of two-dimentional (n x m) array:\n");
    printf("n = ");
    input_column = get_unsigned_integer_number_from_stdin();
    printf("m = ");
    input_row = get_unsigned_integer_number_from_stdin();

    if (input_column < 1 || input_row < 1)
    {
        printf("Each dementional must contain at least one element.\n");
        return EXIT_FAILURE;
    }

    const size_t column_size = input_column;
    const size_t row_size    = input_row;

    int** array = malloc(sizeof *array * column_size);
    for (size_t i = 0; i < column_size; ++i)
        array[i] = malloc(sizeof(int) * row_size);

    for (size_t n = 0; n < column_size; ++n)
    {
        for (size_t m = 0; m < row_size; ++m)
        {
            printf("array[%zu][%zu] = ", n, m);
            array[n][m] = get_integer_number_from_stdin();
        }
        printf("\n");
    }

    int value = 0;
    printf("Enter any number to check the arithmetic mean of each row that "
           "is less than this number: ");
    value = get_integer_number_from_stdin();

    size_t row_counter = 0;
    for (size_t n = 0; n < column_size; ++n)
    {
        int average_sum_of_row = 0;
        for (size_t m = 0; m < row_size; ++m)
        {
            average_sum_of_row += array[n][m];
        }
        if (average_sum_of_row / (int)row_size < value)
            row_counter++;
    }

    printf("Answer:\nThe number of lines with an arithmetic"
           "average less than %d is %zu\n",
           value, row_counter);

    free(array);

    return EXIT_SUCCESS;
}
