#include "input_helper.h"

#include <ctype.h>
#include <string.h>

#define STR_SIZE 256

void remove_digits_multiples_of_three(char* string)
{
    size_t current = 0;
    size_t next    = 0;

    while (string[current])
    {
        // The task only talks about digits, not a word about numbers ;)
        // clang-format off
        if ( string[current] != '3' &&
             string[current] != '6' &&
             string[current] != '9' )
        {
            string[next++] = string[current];
        }
        // clang-format on
        ++current;
    }
    string[next] = '\0';
}

int main(void)
{
    printf("Laboratory work 6\nVariant 11\n");

    char         input_string[STR_SIZE];
    printf("Enter any string:\n");

    if (!fgets(input_string, STR_SIZE, stdin))
    {
        fprintf(stderr, "Incorrect input\n");
        return EXIT_FAILURE;
    }

    size_t length = strlen(input_string) - 1; // delete last '\n'
    if (length % 3 == 0)
    {
        remove_digits_multiples_of_three(input_string);
        printf("The string after deleting all digits "
               "that are multiples of three: \n");
    }
    else
    {
        printf("The string remains the same:\n");
    }

    printf("%s\n", input_string);
    return EXIT_SUCCESS;
}

// string for test
// Hello Wo3rl3d7s1q2
