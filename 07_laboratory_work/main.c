#include "input_helper.h"

#include <ctype.h>
#include <errno.h>
#include <string.h>

int create_file(const char* file_name);
int read_file(const char* file_name);
int add_student_to_file(const char* file_name);
int save_solved_task_to_file(const char* file_name);

void check_errno(void);

enum menu
{
    EXIT,
    CREATE,
    READ,
    ADD,
    VIEW
};

void print_main_menu(void)
{
    // clang-format off
    printf("\n  Main menu:\n"
           "1. Create new file. If file already exist, all data will be deleted.\n"
           "2. Read from file.\n"
           "3. Add information about the new student.\n"
           "4. View the solved task.\n"
           "0. Exit.\n");
    // clang-format on
}

int main(void)
{
    printf("Laboratory work 7\nVariant 11\n");

    const char* file_name = "students.txt";

    int main_loop_run = TRUE;
    while (main_loop_run)
    {
        print_main_menu();
        long user_choise = get_integer_number_from_stdin();

        switch (user_choise)
        {
            case CREATE:
            {
                clear_screen();
                if (create_file(file_name) == EXIT_SUCCESS)
                    break;
                else
                    exit(EXIT_FAILURE);
            }
            case READ:
            {
                clear_screen();
                if (read_file(file_name) == EXIT_SUCCESS)
                    break;
                else
                {
                    printf("File %s doesn't exist.\n\n", file_name);
                    break;
                }
            }
            case ADD:
            {
                clear_screen();
                if (add_student_to_file(file_name) == EXIT_SUCCESS)
                    break;
                else
                {
                    printf("Incorrect input. Try again.\n\n");
                    break;
                }
            }
            case VIEW:
            {
                clear_screen();
                if (save_solved_task_to_file(file_name) == EXIT_SUCCESS)
                    break;
                else
                {
                    printf("Cann't do solve variant task.\n");
                    break;
                }
            }
            case EXIT:
            {
                clear_screen();
                main_loop_run = FALSE;
                break;
            }
            default:
            {
                clear_screen();
                printf("Somthing wrong. Please be careful when choosing menu "
                       "item\n");
                break;
            }
        }
    }

    printf("Have a nice day!\n");
    return EXIT_SUCCESS;
}

int create_file(const char* file_name)
{
    FILE* out_file;
    out_file = fopen(file_name, "w");
    if (!out_file)
    {
        check_errno();
        return EXIT_FAILURE;
    }

    printf("File \"%s\" was created successfully.\n", file_name);
    fclose(out_file);
    return EXIT_SUCCESS;
}

int read_file(const char* file_name)
{
    FILE* in_file;
    in_file = fopen(file_name, "r");
    if (!in_file)
    {
        check_errno();
        return EXIT_FAILURE;
    }

    size_t current_line = 0;
    size_t is_new_line  = 6; // [1..6] fields in student

    size_t line_size = 41; // 41 == strlen("Student: ") + sizeof(student.name)
    char*  line      = malloc(line_size);
    while (getline(&line, &line_size, in_file) != EOF)
    {
        printf("%s", line);
        ++current_line;
        if (is_new_line - current_line == 0)
        {
            current_line = 0;
            printf("\n");
        }
    }
    printf("\nFile \"%s\" was loaded successfully.\n", file_name);
    free(line);
    fclose(in_file);
    return EXIT_SUCCESS;
}

int add_student_to_file(const char* file_name)
{
    printf("Adding information about new student in \"%s\".\n", file_name);

    char          name[32];
    char          group_number[16];
    unsigned long mark_physics          = 0;
    unsigned long mark_math             = 0;
    unsigned long mark_computer_science = 0;
    float         average_mark          = 0.f;

    printf("Name: ");
    fgets(name, sizeof(name), stdin);
    if (name[0] == '\n')
    {
        printf("Name field can't be empty.\n");
        return EXIT_FAILURE;
    }

    printf("Group number: ");
    fgets(group_number, sizeof(group_number), stdin);

    printf("physics mark: ");
    mark_physics = get_unsigned_integer_number_from_stdin();
    if (mark_physics > 100)
    {
        printf("Marks cann't greater than 100\n");
        return EXIT_FAILURE;
    }

    printf("math mark: ");
    mark_math = get_unsigned_integer_number_from_stdin();
    if (mark_math > 100)
    {
        printf("Marks cann't greater than 100\n");
        return EXIT_FAILURE;
    }

    printf("computer science mark: ");
    mark_computer_science = get_unsigned_integer_number_from_stdin();
    if (mark_computer_science > 100)
    {
        printf("Marks cann't greater than 100\n");
        return EXIT_FAILURE;
    }

    average_mark =
        (float)(mark_physics + mark_math + mark_computer_science) / 3.f;

    FILE* out_file;
    out_file = fopen(file_name, "a");
    if (!out_file)
    {
        check_errno();
        exit(EXIT_FAILURE);
    }

    fputs("Student: ", out_file);
    fputs(name, out_file);

    fputs("Group: ", out_file);
    fputs(group_number, out_file);

    char mark_str[3]; // buffer for string representation

    snprintf(mark_str, sizeof(mark_str), "%lu", mark_physics);
    fputs("physics mark: ", out_file);
    fputs(mark_str, out_file);
    fputs("\n", out_file);

    snprintf(mark_str, sizeof(mark_str), "%lu", mark_math);
    fputs("math mark: ", out_file);
    fputs(mark_str, out_file);
    fputs("\n", out_file);

    snprintf(mark_str, sizeof(mark_str), "%lu", mark_computer_science);
    fputs("computer science mark: ", out_file);
    fputs(mark_str, out_file);
    fputs("\n", out_file);

    char average_mark_str[5]; // buffer for string representation
    snprintf(average_mark_str, sizeof(average_mark_str), "%.1f", average_mark);
    fputs("average mark: ", out_file);
    fputs(average_mark_str, out_file);
    fputs("\n", out_file);

    fflush(out_file);

    fclose(out_file);
    return EXIT_SUCCESS;
}

int save_solved_task_to_file(const char* file_name)
{
    int  is_success = FALSE;
    char group_number[16];
    printf("Enter group number: ");
    fgets(group_number, sizeof(group_number), stdin);

    printf("\nSearching students in a group %s"
           "with a computer science mark of 9 or 10...\n\n",
           group_number);

    FILE* in_file;
    in_file = fopen(file_name, "rb");
    if (!in_file)
    {
        check_errno();
        return EXIT_FAILURE;
    }

    int           expected_group = FALSE;
    unsigned long line_count     = 0;
    // max expected size of student block
    char* buffer            = malloc(sizeof(char) * 134);
    buffer[0]               = '\0';
    int can_write_to_buffer = TRUE;
    // 41 == strlen("Student: ") + sizeof(student.name)
    const size_t line_size = 41;
    char*        line      = malloc(line_size);
    while (getline(&line, &line_size, in_file) != EOF)
    {
        if (line_count == 6)
        {
            line_count          = 0;
            can_write_to_buffer = TRUE;
            expected_group      = FALSE;
            if (strlen(buffer) > 1)
            {
                // TODO :
                // write expected students in file here
                //
                printf("%s\n", buffer);
                strcpy(buffer, "\0"); // clear buffer
                is_success = TRUE;
            }
        }

        if (can_write_to_buffer)
            strncat(buffer, line, strlen(line));

        if (strncmp(line, "Group: ", 7) == 0)
        {
            char      group_number_infile[16];
            const int shift = 7;

            for (int position = 0; line[position + shift] != '\n'; ++position)
                group_number_infile[position] = line[position + shift];

            if (strncmp(group_number, group_number_infile,
                        (strlen(group_number) - 1)) == 0)
            {
                expected_group = TRUE;
            }
        }

        if (strncmp(line, "computer science mark: ", 23) == 0)
        {
            if (expected_group)
            {
                long  mark = 0;
                char* str  = line;
                while (*str)
                {
                    if (isdigit(*str))
                        mark = strtol(str, &str, 10);
                    else
                        str++;
                }

                if (mark != 9 && mark != 10)
                {
                    can_write_to_buffer = FALSE;
                    strcpy(buffer, "\0"); // clear buffer
                }
            }
            else
            {
                can_write_to_buffer = FALSE;
                strcpy(buffer, "\0"); // clear buffer
            }
        }

        ++line_count;
    }

    if (!is_success)
        printf("Not found any students from group: %s", group_number);

    free(buffer);
    free(line);
    fclose(in_file);
    return EXIT_SUCCESS;
}

void check_errno(void)
{
    const char* error_msg = "Can't open file ";
    if (errno == ENOENT)
        fprintf(stderr, "%s, no such file or directory\n", error_msg);
    if (errno == EACCES)
        fprintf(stderr, "%s, permission denied\n", error_msg);
    if (errno == EROFS)
        fprintf(stderr, "%s, read-only file system\n", error_msg);
}
