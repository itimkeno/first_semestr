cmake_minimum_required(VERSION 3.5)

project(07_laboratory_work_another LANGUAGES C)

include_directories(${INPUT_HELPER_SOURCE_PATH})

add_executable(07_unit_2 main.c
                         student_list.c
                         student_list.h
                         )
target_link_libraries(07_unit_2 PRIVATE input_helper)
