#include "input_helper.h"
#include "student_list.h"

#include <errno.h>
#include <string.h>

int create_file(const char* file_name);
int read_file(const char* file_name);
int add_student_to_file(const char* file_name);
int save_solved_task_to_file(const char* file_name);

void check_errno(void);

enum menu
{
    EXIT,
    CREATE,
    READ,
    ADD,
    SHOW_TASK
};

// global list of students
static student_node* student_list;

void print_main_menu(void)
{
    // clang-format off
    printf("\n  Main menu:\n"
           "1. Create new file. If file already exist, all data will be deleted.\n"
           "2. Show all students.\n"
           "3. Add information about the new student.\n"
           "4. View the solved task.\n"
           "0. Exit.\n");
    // clang-format on
}

int main(void)
{
    printf("Laboratory work 7 with list\nVariant 11\n");

    const char* file_name = "students.dat";

    int main_loop_run = TRUE;
    while (main_loop_run)
    {
        print_main_menu();
        long user_choise = get_integer_number_from_stdin();

        switch (user_choise)
        {
            case CREATE:
            {
                clear_screen();
                if (create_file(file_name) == EXIT_SUCCESS)
                    break;
                else
                    exit(EXIT_FAILURE);
            }
            case READ:
            {
                clear_screen();
                if (read_file(file_name) == EXIT_SUCCESS)
                {
                    student_list = print_all(student_list, stdout);
                    break;
                }
                else
                {
                    printf("File %s doesn't exist.\n\n", file_name);
                    break;
                }
            }
            case ADD:
            {
                clear_screen();
                if (add_student_to_file(file_name) == EXIT_SUCCESS)
                    break;
                else
                {
                    printf("Incorrect input. Try again.\n\n");
                    break;
                }
            }
            case SHOW_TASK:
            {
                clear_screen();
                if (save_solved_task_to_file(file_name) == EXIT_SUCCESS)
                    break;
                else
                {
                    printf("Cann't do solve variant task.\n");
                    break;
                }
            }
            case EXIT:
            {
                clear_screen();
                main_loop_run = FALSE;
                break;
            }
            default:
            {
                clear_screen();
                printf("Somthing wrong. Please be careful when choosing menu "
                       "item\n");
                break;
            }
        }
    }

    printf("Have a nice day!\n");
    return EXIT_SUCCESS;
}

int create_file(const char* file_name)
{
    FILE* out_file;
    out_file = fopen(file_name, "w");
    if (!out_file)
    {
        check_errno();
        return EXIT_FAILURE;
    }

    printf("File \"%s\" was created successfully.\n", file_name);
    fclose(out_file);
    return EXIT_SUCCESS;
}

int read_file(const char* file_name)
{
    FILE* in_file;
    in_file = fopen(file_name, "r");
    if (!in_file)
    {
        check_errno();
        return EXIT_FAILURE;
    }

    clear(student_list);
    student_list = NULL;

    student* tmp_student = malloc(sizeof(student));

    while (fread(tmp_student, sizeof(student), 1, in_file))
        student_list = push_back(student_list, tmp_student);

    free(tmp_student);
    fclose(in_file);
    return EXIT_SUCCESS;
}

int add_student_to_file(const char* file_name)
{
    printf("Adding information about new student in \"%s\".\n", file_name);

    student* student_object = malloc(sizeof(student));

    printf("Name: ");
    fgets(student_object->name, sizeof(student_object->name), stdin);
    if (student_object->name[0] == '\n')
    {
        free(student_object);
        printf("Name field can't be empty.\n");
        return EXIT_FAILURE;
    }

    printf("Group number: ");
    fgets(student_object->group_number, sizeof(student_object->group_number),
          stdin);

    printf("physics mark: ");
    student_object->marks.physics = get_unsigned_integer_number_from_stdin();
    if (student_object->marks.physics > 100)
    {
        free(student_object);
        printf("Marks cann't greater than 100\n");
        return EXIT_FAILURE;
    }

    printf("math mark: ");
    student_object->marks.math = get_unsigned_integer_number_from_stdin();
    if (student_object->marks.math > 100)
    {
        free(student_object);
        printf("Marks cann't greater than 100\n");
        return EXIT_FAILURE;
    }

    printf("computer science mark: ");
    student_object->marks.computer_science =
        get_unsigned_integer_number_from_stdin();
    if (student_object->marks.computer_science > 100)
    {
        free(student_object);
        printf("Marks cann't greater than 100\n");
        return EXIT_FAILURE;
    }

    student_object->average_mark =
        ((float)student_object->marks.physics +
         (float)student_object->marks.math +
         (float)student_object->marks.computer_science) /
        3.f;

    FILE* out_file;
    out_file = fopen(file_name, "a");
    if (!out_file)
    {
        check_errno();
        free(student_object);
        exit(EXIT_FAILURE);
    }

    if (fwrite(student_object, sizeof(student), 1, out_file) != 0)
        fprintf(stderr, "Student info to file written successfully !\n");
    else
        fprintf(stderr, "Error writing file!\n");

    fclose(out_file);
    free(student_object);
    return EXIT_SUCCESS;
}

int save_solved_task_to_file(const char* file_name)
{
    char group_number[16];
    printf("Enter group number: ");
    fgets(group_number, sizeof(group_number), stdin);

    printf("\nSearching students in a group %s"
           "with a computer science mark of 9 or 10...\n\n",
           group_number);

    if (read_file(file_name) == EXIT_FAILURE)
        return EXIT_FAILURE;

    student_node* expected_students;
    expected_students = find_all_with_good_marks(student_list, group_number);

    if (expected_students != NULL)
    {
        expected_students = print_all(expected_students, stdout);
        //// save in variant_11.txt ////
        FILE* out_file;
        out_file = fopen("variant_11.txt", "w");
        if (!out_file)
        {
            check_errno();
            exit(EXIT_FAILURE);
        }
        expected_students = print_all(expected_students, out_file);
        fclose(out_file);
        ////////////////////////////////
        clear(expected_students);
        expected_students = NULL;
    }
    else
        printf("Not found any students from group: %s", group_number);

    return EXIT_SUCCESS;
}

void check_errno(void)
{
    const char* error_msg = "Can't open file ";
    if (errno == ENOENT)
        fprintf(stderr, "%s, no such file or directory\n", error_msg);
    if (errno == EACCES)
        fprintf(stderr, "%s, permission denied\n", error_msg);
    if (errno == EROFS)
        fprintf(stderr, "%s, read-only file system\n", error_msg);
}
