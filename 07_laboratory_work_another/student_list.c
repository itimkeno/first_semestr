#include "student_list.h"

// return the first node of the list
student_node* push_back(student_node* list, const student* input_student)
{
    student_node* new_student = NULL;
    student_node* head        = list;
    new_student               = malloc(sizeof(student_node));

    strcpy(new_student->_student.name, input_student->name);
    strcpy(new_student->_student.group_number, input_student->group_number);
    new_student->_student.marks.physics = input_student->marks.physics;
    new_student->_student.marks.math    = input_student->marks.math;
    new_student->_student.marks.computer_science =
        input_student->marks.computer_science;
    new_student->_student.average_mark = input_student->average_mark;

    new_student->next_student = NULL;

    if (head)
    {
        head = list;
        while (list->next_student)
        {
            list = list->next_student;
        }
        if (list->next_student == NULL)
        {
            list->next_student = new_student;
            list               = list->next_student;
        }
    }
    else
    {
        head = list = new_student;
    }

    return head;
}

// make a new list based on conditions
student_node* find_all_with_good_marks(const student_node* list,
                                       const char*         group_number)
{
    student_node* return_list = NULL;
    student_node* iter        = list;
    while (iter)
    {
        if (strcmp(iter->_student.group_number, group_number) == 0)
        {
            if (iter->_student.marks.computer_science == 9 ||
                iter->_student.marks.computer_science == 10)
            {
                return_list = push_back(return_list, &iter->_student);
            }
        }
        iter = iter->next_student;
    }
    return return_list;
}

// delete all data from the list
void clear(student_node* list)
{
    student_node* tmp = list;
    while (tmp)
    {
        list = list->next_student;
        free(tmp);
        tmp = list;
    }
    list = NULL;
}

// print all student info, return first node
student_node* print_all(student_node* list, FILE* stream)
{
    student_node* iter = list;
    while (iter)
    {
        fprintf(stream, "Student: %s", iter->_student.name);
        fprintf(stream, "Group: %s", iter->_student.group_number);
        fprintf(stream, "physics mark: %lu\n", iter->_student.marks.physics);
        fprintf(stream, "math mark: %lu\n", iter->_student.marks.math);
        fprintf(stream, "computer science mark: %lu\n",
                iter->_student.marks.computer_science);
        fprintf(stream, "average mark: %.1f\n\n", iter->_student.average_mark);

        iter = iter->next_student;
    }

    return list;
}

// return the first node of the list*
student_node* insert_student(student_node* list, const char* name,
                             const char*         group_number,
                             const unsigned long physics_mark,
                             const unsigned long math_mark,
                             const unsigned long computer_science_mark)
{
    student_node* new_student = NULL;
    student_node* first       = NULL;
    new_student               = malloc(sizeof(student_node));

    strcpy(new_student->_student.name, name);
    strcpy(new_student->_student.group_number, group_number);
    new_student->_student.marks.physics          = physics_mark;
    new_student->_student.marks.math             = math_mark;
    new_student->_student.marks.computer_science = computer_science_mark;
    new_student->_student.average_mark =
        (float)(physics_mark + math_mark + computer_science_mark) / 3.f;

    new_student->next_student = NULL;

    if (list)
    {
        first = list;
        while (list->next_student)
        {
            list = list->next_student;
        }
        if (list->next_student == NULL)
        {
            list->next_student = new_student;
            list               = list->next_student;
        }
    }
    else
    {
        first = list = new_student;
    }

    free(new_student);
    return first;
}
