#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct student_marks
{
    unsigned long physics;
    unsigned long math;
    unsigned long computer_science;
} typedef marks;

struct student
{
    char  name[32];
    char  group_number[16];
    marks marks;
    float average_mark;
} typedef student;

struct student_node
{
    student              _student;
    struct student_node* next_student;
} typedef student_node;

// return the first node of the list
student_node* push_back(student_node* list, const student* new_student);
// make a new list based on conditions
student_node* find_all_with_good_marks(const student_node* list,
                                       const char*         group_number);
// delete all data from the list
void clear(student_node* list);
// print all student info, return first node
student_node* print_all(student_node* list, FILE* stream);
// return the first node of the list
student_node* insert_student(student_node* list, const char* name,
                             const char*         group_number,
                             const unsigned long physics_mark,
                             const unsigned long math_mark,
                             const unsigned long computer_science_mark);
