#include <math.h>
#include <stdio.h>

#include "input_helper.h"

double sum_of_x_func(double x, size_t n)
{
    double recurrence_relation = 1.0; //(x + x) / 2.0;
    double sum_of_x            = 1.0; // recurrence_relation;
    for (size_t k = 0; k < n; ++k)
    {
        // clang-format off
        recurrence_relation *= ((x * (double)(k + 1) * (double)(k + 1) + x) /
                               ((double)(k + 1) * (double)(k * k + 1) * 2.0));
        sum_of_x += recurrence_relation;
        // clang-format on
    }

    return sum_of_x;
}

double y_of_x_func(double x)
{
    return (((x * x) / 4) + (x / 2) + 1) * exp(x / 2);
}

void output_sum(double sum_of_x(double, size_t), double x, size_t n)
{
    printf("S(x) = %lf\n", sum_of_x(x, n));
}
void output_y(double y_of_x(double), double x)
{
    printf("Y(x) = %lf\n", y_of_x(x));
}
void output_abs(double sum_of_x(double, size_t), double y_of_x(double),
                double x, size_t n)
{
    printf("|Y(x) - S(x)| = %lf\n", fabs(sum_of_x(x, n) - y_of_x(x)));
}

int main()
{
    int    function_choiser = 0;
    double x                = 0.0;
    printf("Laboratory work 8\nVariant 11\n");
    printf("Enter number for chose function:\n"
           "1 : S(x)\n"
           "2 : Y(x)\n"
           "3 : |Y(x) - S(x)|\n");

    function_choiser = get_integer_number_from_stdin();

    switch (function_choiser)
    {
        case 1:
        {
            size_t n = 0;
            printf("Selected function : S(x)\n");
            printf("Enter x : ");
            x = get_real_number_from_stdin();
            printf("Enter iteration count \"n\" : ");
            n = get_unsigned_integer_number_from_stdin();

            output_sum(sum_of_x_func, x, n);
            break;
        }
        case 2:
        {
            printf("Selected function : Y(x)\n");
            printf("Enter x : ");
            x = get_real_number_from_stdin();

            output_y(y_of_x_func, x);
            break;
        }
        case 3:
        {
            size_t n = 0;
            printf("Selected function : |Y(x) - S(x)|\n");
            printf("Enter x : ");
            x = get_real_number_from_stdin();
            printf("Enter iteration count \"n\" : ");
            n = get_unsigned_integer_number_from_stdin();

            output_abs(sum_of_x_func, y_of_x_func, x, n);
            break;
        }
        default:
        {
            fprintf(stderr, "This function does not exist\n");
            exit(1);
        }
    }

    return 0;
}
