## First semestr BSUIR FIK
# Laboratory works Variant 11

This project contains 8 lab works

Language: [C]                                              
Build system: [CMake] (3.5 or higher) and [ninja] or [make]

Steps to build
```
  Set project folder as root

  mkdir build 
  cd build
  cmake ..
  cmake --build .
```

You can also open this project as a folder in any IDE with built-in CMake support.

[C]: https://en.wikipedia.org/wiki/C_(programming_language)
[CMake]: https://cmake.org/
[ninja]: https://ninja-build.org/
[make]: https://www.gnu.org/software/make/
